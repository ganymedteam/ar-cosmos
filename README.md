# AR Cosmos

AR Cosmos eases the astronomical concepts like the size comparison of the planets, rotation movement and the gravity effect of these massive objects.

#### Abstract

AR Cosmos is an augmented reality application that is made by using the game engine (Unity) and the augmented reality SDK (Vuforia). The AR application is about augmenting some planet cards (namely image targets) into 3D model planets which can help the user to understand and visualize the astronomical concepts in a better way like the massive size differences between these objects. Altogether there are currently 9 planets (all Milky Way planets) that have already been augmented and each one of these planets offers a different type of interactivity by using a virtual button, for example, planet Mercury offers you the possibility to compare its size to the Sun and Planet Pluto offers you some facts about Pluto. The application can be used by any android smartphone and like any other AR application it will launch a camera once the application is runned. By pointing the camera on the planet cards (the image targets) 3D models and virtual buttons will pop out on the pointed planet card and the user can now interact with it by pressing the button or releasing it.

#### Project Concept

The concept of this project is an AR application to improve children interactions with astronomical objects: the idea of the project is to make an augmented learning environment (AR application) that is targeted to teach astronomy to children in schools, therefore I will transform a board (made of Carton) that has upon it some markers (which are Planets cards) into a 3D solar system like Planets and Sun (3D objects) which are already available on the Unity market for free. In the picture below we see some of the image targets in reality that are used in the application.

![](Images/Image.jpg)

The application should enhance the imagination of the children to visualize the planets, the sun and the rotation movements of these objects. That means when the objects occur in the camera then they will be in a rotation mode. Each one of these image targets (planet cards) will be provided by a virtual button that can either scale the size of the 3D objects (make it bigger or smaller) or can add some other visual contents (e.g. like showing the moon of some planets). That means a user or the children can just interact with the system by changing the size, showing some facts about the planet or even compare the size of some of these planets. The users can also move the markers (that represents the planets) to mimic the effect of the gravity that bigger objects have on the smaller ones. Since grasping astronomical ideas were always challenging for the children due to the size of the objects, this application will indeed help them to see with their naked eyes how these objects differ from each other from a size and appearance point of view and beside that the application will bring a lot of amusement, fun and Technology to the classroom. The picture below shows the covered image targets with the 3D models in the scene.

![](Images/FullScene.jpg)

#### Future Work

The project can be further extended to some other planets and other functionalities.

![](Images/Image2.jpg)

#### Video Link

https://www.youtube.com/watch?v=JVkhPnFhdYM&t=99s
