using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class PlutoScript : MonoBehaviour, IVirtualButtonEventHandler
{

    public GameObject vBtObject;
    public GameObject Text;

    // Start is called before the first frame update
    void Start()
    {
        // vBtObject = GameObject.Find("VenusVB");
        vBtObject.GetComponent<VirtualButtonBehaviour>().RegisterEventHandler(this);

        Text = GameObject.Find("PlutoInfo");
        Text.SetActive(false);
    }

    public void OnButtonPressed(VirtualButtonBehaviour vb)
    {
        Text.SetActive(true);
    }

    public void OnButtonReleased(VirtualButtonBehaviour vb)
    {
        Text.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

    }
}