using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class EarthScript : MonoBehaviour, IVirtualButtonEventHandler
{

    public GameObject vBtObject;
    public GameObject Moon;

    // Start is called before the first frame update
    void Start()
    {
        // vBtObject = GameObject.Find("VenusVB");
        vBtObject.GetComponent<VirtualButtonBehaviour>().RegisterEventHandler(this);

        Moon = GameObject.Find("Moon");
        Moon.SetActive(false);
    }

    public void OnButtonPressed(VirtualButtonBehaviour vb)
    {
        Moon.SetActive(true);
    }

    public void OnButtonReleased(VirtualButtonBehaviour vb)
    {
        Moon.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

    }
}

