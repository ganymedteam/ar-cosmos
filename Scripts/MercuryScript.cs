using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class MercuryScript : MonoBehaviour, IVirtualButtonEventHandler
{

    public GameObject vBtObject;
    public GameObject Sun;

    // Start is called before the first frame update
    void Start()
    {
        // vBtObject = GameObject.Find("VenusVB");
        vBtObject.GetComponent<VirtualButtonBehaviour>().RegisterEventHandler(this);

        Sun = GameObject.Find("Sun");
        Sun.SetActive(false);
    }

    public void OnButtonPressed(VirtualButtonBehaviour vb)
    {
        Sun.SetActive(true);
    }

    public void OnButtonReleased(VirtualButtonBehaviour vb)
    {
        Sun.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

    }
}