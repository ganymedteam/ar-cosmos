using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoonScript : MonoBehaviour
{
    public float TimeCounter = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        TimeCounter += Time.deltaTime;

        float x = Mathf.Cos(TimeCounter);
        float y = Mathf.Sin(TimeCounter);
        float z = 0;

        transform.position = new Vector3(x, y, z);
        
    }
}
