using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class UranusScript : MonoBehaviour, IVirtualButtonEventHandler
{
    public GameObject vBtObject1;

    public GameObject Model1;
    public GameObject Model2;

    // Start is called before the first frame update
    void Start()
    {
        vBtObject1.GetComponent<VirtualButtonBehaviour>().RegisterEventHandler(this);

        Model2 = GameObject.Find("Alien");
        Model1 = GameObject.Find("Uranus");
        Model1.SetActive(true);
        Model2.SetActive(false);
    }

    public void OnButtonPressed(VirtualButtonBehaviour vb)
    {
        Model1.SetActive(false);
        Model2.SetActive(true);

    }

    public void OnButtonReleased(VirtualButtonBehaviour vb)
    {
        Model2.SetActive(false);
        Model1.SetActive(true);

    }


    // Update is called once per frame
    void Update()
    {


    }
}