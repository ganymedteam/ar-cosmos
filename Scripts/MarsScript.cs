using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class MarsScript : MonoBehaviour
{
    
    public GameObject Earth;
    public float speed = 0;
    // Start is called before the first frame update
    void Start()
    {
      
    }

    void Update()
    {
        Orbit();
    }

    void Orbit ()
    {
        transform.RotateAround(Earth.transform.position, Vector3.up, speed * Time.deltaTime);
    }
}
